

#import <UIKit/UIKit.h>
#import "Cliente.h"

@interface MenuTableViewController : UITableViewController <UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) IBOutlet UITableView *tableViewMenu;
@property (weak, nonatomic) IBOutlet UILabel *lbCarregando;

@end
