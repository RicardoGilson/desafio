

#import <UIKit/UIKit.h>
#import "Produto.h"
#import "EnumType.h"

@interface PedidoViewController : UIViewController

@property (strong, nonatomic) Produto *produto;

// View de Controle
@property (weak, nonatomic) IBOutlet UIView *viewTamanhoCafe;
@property (weak, nonatomic) IBOutlet UIView *viewAcucar;
@property (weak, nonatomic) IBOutlet UIView *viewAdicionais;

@property (weak, nonatomic) IBOutlet UIImageView *img_produto;
@property (weak, nonatomic) IBOutlet UILabel *lbProduto;


// Escolha do tamanho do café
@property (weak, nonatomic) IBOutlet UIButton *btn_size_little;
@property (weak, nonatomic) IBOutlet UIButton *btn_size_middle;
@property (weak, nonatomic) IBOutlet UIButton *btn_size_big;

// Escolha da quantidade do acuçar
@property (weak, nonatomic) IBOutlet UIButton *btn_no_sugar;
@property (weak, nonatomic) IBOutlet UIButton *btn_sugar_1;
@property (weak, nonatomic) IBOutlet UIButton *btn_sugar_2;
@property (weak, nonatomic) IBOutlet UIButton *btn_sugar_3;

// Quantidade
@property (weak, nonatomic) IBOutlet UIButton *btn_qtd_less;
@property (weak, nonatomic) IBOutlet UIButton *btn_qtd_plus;
@property (weak, nonatomic) IBOutlet UILabel *lb_qtd_pedida;

// Adiciona no carrinho
@property (weak, nonatomic) IBOutlet UIButton *btn_add_car;

@property (nonatomic) int qtd_pedido;

@end
