

#import <UIKit/UIKit.h>
#import "Produto.h"

@interface ListaCellCarrinho : UITableViewCell{
    Produto *produtoDisponivel;
}
@property (weak, nonatomic) IBOutlet UIImageView *imgPedido;
@property (weak, nonatomic) IBOutlet UILabel *lbDescricao;
@property (weak, nonatomic) IBOutlet UILabel *lbQuantidade;
@property (weak, nonatomic) IBOutlet UILabel *lbAdicionais;
-(void)setItemMenu:(Produto *)produto;

@end
