

#import <UIKit/UIKit.h>
#import "Produto.h"

@interface ListaCellMenu : UITableViewCell{
    Produto *produtoDisponivel;
}
@property (weak, nonatomic) IBOutlet UIImageView *imgMenu;
@property (weak, nonatomic) IBOutlet UILabel *lbDescricao;
-(void)setItemMenu:(Produto *)produto;

@end
