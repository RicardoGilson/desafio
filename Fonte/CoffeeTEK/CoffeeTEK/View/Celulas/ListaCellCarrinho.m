
#import "ListaCellCarrinho.h"


@interface ListaCellCarrinho()

@end

@implementation ListaCellCarrinho

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

// Recebendo os Itens de Pedido Dinamicamente
-(void)setItemMenu:(Produto *)produto{

    produtoDisponivel = produto;
    _lbDescricao.text = produtoDisponivel.titulo;
    _imgPedido.image = [UIImage imageWithData:produtoDisponivel.imagem];
    _lbQuantidade.text = [NSString stringWithFormat:@"%d", produtoDisponivel.quantidade];
    
    // Percorre todo os adicionais
    NSMutableString *adicionais = [[NSMutableString alloc] init];
    for (NSString *valor in produto.adicional) {
        [adicionais appendString: [NSString stringWithFormat:@"%@ ", valor]];
    }
    _lbAdicionais.text  = adicionais;
    
}
@end
