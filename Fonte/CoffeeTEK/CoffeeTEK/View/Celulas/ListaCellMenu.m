
#import "ListaCellMenu.h"


@interface ListaCellMenu()

@end

@implementation ListaCellMenu

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

// Recebendo os Itens de Menu Dinamicamente
-(void)setItemMenu:(Produto *)produto{

    produtoDisponivel = produto;
    _lbDescricao.text = produtoDisponivel.titulo;
    _imgMenu.image = [UIImage imageWithData:produtoDisponivel.imagem];
}
@end
