
#import "MenuTableViewController.h"
#import "Cliente.h"
#import "Celulas/ListaCellMenu.h"
#import "UIImageView+AFNetworking.h"
#import "AFHTTPRequestOperation.h"
#import "Util.h"
#import "PedidoViewController.h"


@interface MenuTableViewController ()

@end

@implementation MenuTableViewController

static NSString * const URL = @"https://desafio-mobility.herokuapp.com/";


- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Iniciando Sessão do Usuário
    [Cliente sessaoAtiva];
    
    if (haConectividade()) {
        // Obter dados do link
        [self getDados];
    } else {
        
        UIAlertController *alerta = [UIAlertController alertControllerWithTitle:@"Erro na requisição" message:@"Algo aconteceu durante a requisição. Verifique sua conexão de internet.\n\n Atualize novamente puxando para baixo!" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction * action) {}];
        [alerta addAction:defaultAction];
        [self presentViewController:alerta animated:YES completion:nil];
    }
    
    // Inicializa o controle de refresh dos dados
    self.refreshControl = [[UIRefreshControl alloc] init];
    self.refreshControl.backgroundColor = [UIColor purpleColor];
    self.refreshControl.tintColor = [UIColor whiteColor];
    [self.refreshControl addTarget:self action:@selector(getDados)forControlEvents:UIControlEventValueChanged];
    
}

// Ação que recupera os dados do Link
- (void)getDados
{
    @try {
        
        // 1
        NSString *string = [NSString stringWithFormat:@"%@products.json", URL];
        NSURL *url = [NSURL URLWithString:string];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        
        // 2
        AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
        operation.responseSerializer = [AFJSONResponseSerializer serializer];
        
        [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            // 3
            // Realiza o tratamento dos dados recebidos
            [[Cliente sessaoAtiva] mepearAtributos: (NSDictionary *)responseObject];
            [self->_tableViewMenu reloadData];
            
            // Encerra a atualização do produtos
            [self.refreshControl endRefreshing];
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
            // 4
            // Encerra a atualização do produtos
            [self.refreshControl endRefreshing];
            
            UIAlertController *alerta = [UIAlertController alertControllerWithTitle:@"Erro na requisição" message:@"Algo aconteceu durante a requisição. Verifique sua conexão de internet.\n\n Atualize novamente puxando para baixo!" preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                                  handler:^(UIAlertAction * action) {}];
            [alerta addAction:defaultAction];
            [self presentViewController:alerta animated:YES completion:nil];
            
        }];
        
        // 5
        [operation start];
        
    } @catch (NSException *exception) {
        UIAlertController *alerta = [UIAlertController alertControllerWithTitle:@"Ops" message:@"Não foi possível carregador os dados no momento, tente novamente mais tarde!" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction * action) {}];
        [alerta addAction:defaultAction];
        [self presentViewController:alerta animated:YES completion:nil];
    }
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    // Controla a mensagem quando conteúdo é verificado
    [_lbCarregando setHidden: ([[Cliente sessaoAtiva].meusProdutos count] == 0) ? false : true];
    
    return [[Cliente sessaoAtiva].meusProdutos count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ListaCellMenu *itemMenu = (ListaCellMenu*) [tableView dequeueReusableCellWithIdentifier:@"ListaCellMenu"];
    if (itemMenu == nil) {
        //     agendaCell = [[GrupoCell alloc]init];
        itemMenu = [[[NSBundle mainBundle]loadNibNamed:@"ListaCellMenu" owner:tableView options:nil] objectAtIndex:0];
    }
    
    Produto *produtoAtual =  [[Cliente sessaoAtiva].meusProdutos objectAtIndex:indexPath.row];
    
    [itemMenu setItemMenu:produtoAtual];
    
    return itemMenu;
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"criarPedido"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        PedidoViewController *pedido = segue.destinationViewController;
        pedido.produto = [[Cliente sessaoAtiva].meusProdutos objectAtIndex:indexPath.row];;
    }
}

@end
