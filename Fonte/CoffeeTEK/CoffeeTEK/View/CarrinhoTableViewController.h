

#import <UIKit/UIKit.h>
#import "Cliente.h"

@interface CarrinhoTableViewController : UITableViewController <UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) IBOutlet UITableView *tableViewCarrinho;
@property (weak, nonatomic) IBOutlet UILabel *lbAvisoSemProduto;

@end
