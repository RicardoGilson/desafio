
#import "PedidoViewController.h"
#import "Cliente.h"
#import "EnumType.h"
@interface PedidoViewController ()

@end

@implementation PedidoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Exibindo dados do objeto recebido
    _lbProduto.text     = _produto.titulo;
    _img_produto.image  = [UIImage imageWithData:_produto.imagem];
    _qtd_pedido = 0;
    
    // Cria uma borda arredondada no botão
    _btn_add_car.layer.cornerRadius = 8.0;
    
    [self carregarAtributosPadrao:_produto];
    
}

// Faz o pré-carregamento dos dados conforme valores recebidos
-(void) carregarAtributosPadrao:(Produto*) produto
{
    
    // PRÉ-carregamento do tamanho do café
    for (UIControl *view in _viewTamanhoCafe.subviews) {
        if ([view isKindOfClass:[UIButton class]]) {
            if (produto.tamanho == (int)view.tag) {
                view.alpha = 1;
            }
        }
    }
    
    // PRÉ-carregamento da quantidade de acucar
    for (UIControl *view in _viewAcucar.subviews) {
        if ([view isKindOfClass:[UIButton class]]) {
            if (produto.acucar == (int)view.tag) {
                view.alpha = 1;
            }
        }
    }
    
    // Preenche os itens adicionais
    [self carregarOsAdicionais: produto];
    
}

// Faz o carregamento dos adicionais recebidos para cada produtos
-(void) carregarOsAdicionais:(Produto*) produto
{
    int posicalInicial = 112; // Posição inicial dos componentes
    int distancia = 20; // Espaço entre os itens
    int incremento = 32; // largura do item
    int posicaoAnterior = posicalInicial;
    
    // Percorre todo os adicionais
    for (NSString *valor in produto.adicional) {
    
        // Criando as imagens dinamicamente
        UIImageView *imgItem =[[UIImageView alloc] initWithFrame:CGRectMake(posicalInicial,10,32,32)];
        NSString *nomeImagem = nil;
        // Calculo do incremento para o próximo item na posição correta
        posicaoAnterior += distancia + incremento;
        posicalInicial = posicaoAnterior;
        
        // Procura pelo Enum correspondente
        TIPO_ADICIONAL tipo = [EnumType associarAdicionaisEnumeradores:valor];
        
        // Verificando qual imagem carregar
        switch (tipo) {
            case COFFEE:
                nomeImagem = @"coffe.png";
                break;
            case CHOCOLATE:
                nomeImagem = @"chocolate.png";
                break;
            case CINNAMON:
                nomeImagem = @"cinnamon.png";
                break;
            case MILK:
                nomeImagem = @"milk.png";
                break;
            case CHANTILY:
                nomeImagem = @"cream.png";
                break;
        }
        
        // Adicionando a imagem no componente visual
        imgItem.image=[UIImage imageNamed:nomeImagem];
        [_viewAdicionais addSubview:imgItem];
    }
}


// Escolha do tamanho do café
- (IBAction)escolhaTamanhoCafe:(id)sender {
    
    UIButton *btnescolhido  = (UIButton *) sender;
    btnescolhido.alpha      = 1;
    
    if (btnescolhido.tag == PEQUENO) {
        _btn_size_middle.alpha = 0.5;
        _btn_size_big.alpha = 0.5;
    } else if (btnescolhido.tag == MEDIO)
    {
        _btn_size_big.alpha = 0.5;
        _btn_size_little.alpha = 0.5;
    }
    else if (btnescolhido.tag == GRANDE)
    {
        _btn_size_middle.alpha = 0.5;
        _btn_size_little.alpha = 0.5;
    }
    
    // Capturando a escolha
    _produto.tamanho = (int)btnescolhido.tag;
    
}

// Escolha da quantidade de acucar
- (IBAction)escolhaQuantidadeAcucar:(id)sender {
    
    UIButton *btnescolhido  = (UIButton *) sender;
    btnescolhido.alpha      = 1;
    
    if (btnescolhido.tag == SEM_ACUCAR) {
        _btn_sugar_1.alpha = 0.5;
        _btn_sugar_2.alpha = 0.5;
        _btn_sugar_3.alpha = 0.5;
        
    } else if (btnescolhido.tag == UMA_PEDRA)
    {
        _btn_no_sugar.alpha = 0.5;
        _btn_sugar_2.alpha = 0.5;
        _btn_sugar_3.alpha = 0.5;
    }
    else if (btnescolhido.tag == DUAS_PEDRA)
    {
        _btn_no_sugar.alpha = 0.5;
        _btn_sugar_1.alpha = 0.5;
        _btn_sugar_3.alpha = 0.5;
    }
    else if (btnescolhido.tag == TRES_PEDRA)
    {
        _btn_no_sugar.alpha = 0.5;
        _btn_sugar_1.alpha = 0.5;
        _btn_sugar_2.alpha = 0.5;
    }
    
    // Capturando a escolha
    _produto.acucar = (int)btnescolhido.tag;
}

// Escolha da quantidade pedida
- (IBAction)quantidadePedido:(id)sender {
    UIButton *btnQuantidade = sender;
    
    if (btnQuantidade.tag == 1) {
        _qtd_pedido += (int) btnQuantidade.tag;
    }
    else
    {
        // Garante que não dimunuirá uma quantidade menor que a permitida
        if (_qtd_pedido >= 1) {
            _qtd_pedido--;
        }
    }
    
    // Armazenando a quantidade solicitada
    _produto.quantidade = _qtd_pedido;
    
    _lb_qtd_pedida.text = [NSString stringWithFormat:@"%d", _qtd_pedido];
}

// Adiciona o pedido no carrinho de compras
- (IBAction)adicionarPedidoCarrinho:(id)sender {

    UIAlertController *alerta = nil;
    
    // Verificando se a quantidade foi escolhida
    if (_qtd_pedido > 0) {
        
        [[Cliente sessaoAtiva].meusPedidos addObject: _produto];
        
        alerta = [UIAlertController alertControllerWithTitle:@"Sucesso" message:@"Seu pedido foi adicionado no carrinho!" preferredStyle:UIAlertControllerStyleAlert];
    }else
    {
        alerta = [UIAlertController alertControllerWithTitle:@"Ops" message:@"Você precisa escolher uma quantidade!" preferredStyle:UIAlertControllerStyleAlert];
    }
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {}];
    [alerta addAction:defaultAction];
    [self presentViewController:alerta animated:YES completion:nil];
}

@end
