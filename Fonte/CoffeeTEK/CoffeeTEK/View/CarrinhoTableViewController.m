
#import "CarrinhoTableViewController.h"
#import "Cliente.h"
#import "Celulas/ListaCellCarrinho.h"
#import "Util.h"
#import "Produto.h"

@interface CarrinhoTableViewController ()

@end

@implementation CarrinhoTableViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    
    _lbAvisoSemProduto.text = @"Você não possui nenhum produto\ndentro do seu carrinho.";
    
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    // Controla a mensagem quando conteúdo é verificado
    [_lbAvisoSemProduto setHidden: ([[Cliente sessaoAtiva].meusPedidos count] == 0) ? false : true];
    
    return [[Cliente sessaoAtiva].meusPedidos count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ListaCellCarrinho *itemCarrinho = (ListaCellCarrinho*) [tableView dequeueReusableCellWithIdentifier:@"ListaCellCarrinho"];
    if (itemCarrinho == nil) {
        itemCarrinho = [[[NSBundle mainBundle]loadNibNamed:@"ListaCellCarrinho" owner:tableView options:nil] objectAtIndex:0];
    }
    
    Produto *pedidoAtual =  [[Cliente sessaoAtiva].meusPedidos objectAtIndex:indexPath.row];
    
    [itemCarrinho setItemMenu:pedidoAtual];
    
    return itemCarrinho;
}

-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{

    // Removendo o pedido da lista
    [[Cliente sessaoAtiva].meusPedidos  removeObjectAtIndex:indexPath.row];
    
    // Recarregando a lista
    [_tableViewCarrinho reloadData];
    
}

@end
