#import <Foundation/Foundation.h>
#import <SystemConfiguration/SystemConfiguration.h>
#import <UIKit/UIKit.h>

@interface Util : NSObject

// Métodos
+(NSString*)transformarCampoNULLstring :(id)campo;
+(UIColor *)corCorrespondente:(NSString*)corEmHexa;

BOOL haConectividade();

@end
