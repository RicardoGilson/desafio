#import "EnumType.h"

@implementation EnumType

// Realiza a associação do texto para Enumeradores
+(TIPO_ADICIONAL)associarAdicionaisEnumeradores :(NSString*)valor{
    
    TIPO_ADICIONAL tipoAdicional;
    
    if ([valor isEqualToString:@"coffee"]) {
        tipoAdicional = COFFEE;
    }else if([valor isEqualToString:@"chocolate"]) {
        tipoAdicional = CHOCOLATE;
    }else if([valor isEqualToString:@"cinnamon"]) {
        tipoAdicional = CINNAMON;
    }else if([valor isEqualToString:@"milk"]) {
        tipoAdicional = MILK;
    }else{
        tipoAdicional = CHANTILY;
    }
    return tipoAdicional;
}

@end
