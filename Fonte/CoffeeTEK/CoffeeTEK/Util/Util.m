#import "Reachability.h"
#import "Util.h"

@implementation Util

+(NSString*)transformarCampoNULLstring :(id)campo{
    
    NSString *retorno = @"";
    (([campo isEqual:nil]) || (campo == (id)[NSNull null])) ? (retorno = @"") : (retorno = [NSString stringWithFormat:@"%@",campo]);
    return retorno;
}

// Com base no Hexadecimal informado, define sua cor em RGB
+(UIColor *)corCorrespondente:(NSString*)corEmHexa{
    
    int r, g, b;
    
    if (corEmHexa) {
        sscanf([corEmHexa UTF8String], "%2x%2x%2x", &r, &g, &b);
        return [UIColor colorWithRed:(r/255.0) green:(g/255.0) blue:(b/255.0) alpha:1.0];
    }
    return nil;
}

@end

BOOL haConectividade() {
    Reachability *reachability  = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return networkStatus != NotReachable;

}
