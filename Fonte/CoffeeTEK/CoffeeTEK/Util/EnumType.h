#import <UIKit/UIKit.h>

// ENUMERADORES
typedef enum{
    PEQUENO = 0,
    MEDIO = 1,
    GRANDE = 2
}TAM_CAFE;

typedef enum{
    SEM_ACUCAR = 0,
    UMA_PEDRA = 1,
    DUAS_PEDRA = 2,
    TRES_PEDRA = 3
}QTD_ACUCAR;

typedef enum {
    COFFEE = 0,
    CHOCOLATE = 1,
    CINNAMON = 2,
    MILK = 3,
    CHANTILY = 4
}TIPO_ADICIONAL;



@interface EnumType : NSObject

@property () TAM_CAFE tamanho_cafe;
@property () QTD_ACUCAR qtd_acucar;
@property (nonatomic) TIPO_ADICIONAL tipo_adicional;

+(TIPO_ADICIONAL)associarAdicionaisEnumeradores :(NSString*)valor;


@end
