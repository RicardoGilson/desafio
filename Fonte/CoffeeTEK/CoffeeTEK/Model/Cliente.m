#import "Cliente.h"
#import "Produto.h"
#import "Util.h"

@implementation Cliente

// Inicializa a sessão
static Cliente* _cliente;

+ (Cliente *) sessaoAtiva {
    if (!_cliente) {
        _cliente = [[Cliente alloc] init];
    }
    return _cliente;
}

// Inicializa os objetos da sessão
- (id)init {
    
    self = [super init];
    
    if (self) {
        _meusProdutos  = [[NSMutableArray alloc] init];
        _meusPedidos   = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void)mepearAtributos:(NSDictionary *)dicionario{
    
    @try {
        
        NSMutableArray * produtosTemp = [[NSMutableArray alloc] init];
        
        NSMutableDictionary *produtos = [dicionario objectForKey:@"products"];
        
        Produto *produto = nil;
        
        // Identificando a lista de produtos
        for (NSDictionary *dict in produtos) {
            
            // Obtendo dados do Produto
            produto    = [[Produto alloc] init];
            produto.titulo      = [Util transformarCampoNULLstring:[dict objectForKey:@"title"]];
            produto.tamanho     = [[dict objectForKey:@"size"] intValue];
            produto.acucar      = [[dict objectForKey:@"sugar"] intValue];
            
            NSData* data        = [[NSData alloc] initWithBase64EncodedString:[[dict objectForKey:@"image"] stringByReplacingOccurrencesOfString:@"data:image/png;base64," withString:@""] options:0];
            produto.imagem      = data;
            produto.adicional   = [dict objectForKey:@"additional"];
            
            [produtosTemp addObject:produto];
            
        }
        
        // EVITA DUPLICIDADE CASO OCORRA ERRO NA REQUISIÇÃO
        if (produtosTemp.count > 0) {
            
            // Remove todas as anteriores
            [[Cliente sessaoAtiva].meusProdutos removeAllObjects];
            
            // Adicionando os dados
            [Cliente sessaoAtiva].meusProdutos =  produtosTemp;
        }
        
    } @catch (NSException *exception) {
        @throw;
    } @finally {
    }
    
}

@end
