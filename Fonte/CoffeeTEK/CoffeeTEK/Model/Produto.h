
#import <Foundation/Foundation.h>

@interface Produto : NSObject

@property (nonatomic, strong) NSString *titulo;
@property (nonatomic) int tamanho, acucar, quantidade;
@property (nonatomic, strong) NSData *imagem;
@property (nonatomic, strong) NSDictionary *adicional;


-(id)init;

@end
