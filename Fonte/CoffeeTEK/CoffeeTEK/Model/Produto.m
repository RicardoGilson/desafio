#import "Produto.h"

@implementation Produto

-(id)init{
    self = [super init];

    if (self) {
        _titulo     = [[NSString alloc] init];
        _imagem     = [[NSData alloc] init];
        _adicional  = [[NSDictionary alloc] init];
    }
    return self;
}

@end
