#import <Foundation/Foundation.h>
#import "AFHTTPSessionManager.h"

@protocol ClientDelegate;
@interface Cliente : AFHTTPSessionManager

+ (Cliente *) sessaoAtiva;
@property (nonatomic, weak) id<ClientDelegate> delegate;
@property (nonatomic, strong) NSMutableArray *meusProdutos;
@property (nonatomic, strong) NSMutableArray *meusPedidos;

- (id)init;
- (void)mepearAtributos:(NSDictionary *)dicionario;
@end
